slick-conky
===========

My Personal Ubuntu based Linux distribution : elementaryOS desktop conky configuration file

Required fonts :
- ZegoeUI - U
- Eurostile
- PizzadudeBullets
- Poky
- Stylebats

- Download link for fonts : https://copy.com/RBP3OeQu6BGk
- Download eOS logo : https://copy.com/RBP3OeQu6BGk

Steps for installation :
- Download fonts from link given above & install it (or simply put in ~/.fonts directory)
- Download eOS logo from above link (PS: Logo is not made by me, i have picked up from elementaryos.org sticker, http://elementaryos.org/uploads/content/journal/101--gybHxTUjY.png)
- Place downloaded eOS logo in /Home directory
- Now in /Home directory create a folder named 'scripts'
- Download .draw_bg.lua & put in 'scripts' folder
- Download .conkyrc & put in /Home directory
- Open terminal & type conky
- That's it.


To automatically start conky when computer starts,
- Download start_conky.sh & put in 'scripts' folder
- Right click on start_conky.sh & make it 'executable'
- Now open settings -> startup application
- Click on 'Add'
- In name, write conky
- In command option, click on browse & locate start_conky.sh (i.e /Home/scripts & then click on startconky shell script file)
- The click on add.
- That's it.
